import { Module } from '@nestjs/common';
import { SalarysService } from './salarys.service';
import { SalarysController } from './salarys.controller';

@Module({
  controllers: [SalarysController],
  providers: [SalarysService],
})
export class SalarysModule {}
