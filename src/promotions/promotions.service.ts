import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Promotion } from './entities/promotion.entity';
import { Repository } from 'typeorm';
import { PromotionDetail } from './entities/promotionDetail';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class PromotionsService {
  @InjectRepository(Promotion)
  private promotionRepository: Repository<Promotion>;
  @InjectRepository(PromotionDetail)
  private promotionDetailRepository: Repository<PromotionDetail>;
  @InjectRepository(Product) private productsRepository: Repository<Product>;
  async create(createPromotionDto: CreatePromotionDto) {
    const promotion = new Promotion();
    promotion.condition = createPromotionDto.condition;
    promotion.start = new Date(createPromotionDto.start);
    promotion.end = new Date(createPromotionDto.end);
    const date = new Date();
    if (promotion.start < date && promotion.end > date) {
      promotion.status = true;
    } else {
      promotion.status = false;
    }
    promotion.promotionDetails = [];
    promotion.name = createPromotionDto.name;
    promotion.discount = createPromotionDto.discount;
    const me = JSON.parse(createPromotionDto.promotionDetail.toString());
    for (const io of me) {
      const promoDetail = new PromotionDetail();
      promoDetail.product = await this.productsRepository.findOneBy({
        id: io.productId,
      });
      promoDetail.productName = io.productName;
      promoDetail.promoName = createPromotionDto.name;
      await this.promotionDetailRepository.save(promoDetail);
      promotion.promotionDetails.push(promoDetail);
    }
    if (createPromotionDto.image && createPromotionDto.image !== '') {
      promotion.image = createPromotionDto.image;
    }
    return this.promotionRepository.save(promotion);
  }

  findAll() {
    return this.promotionRepository.find();
  }

  async findDetail(name: string) {
    return await this.promotionDetailRepository.find({
      where: { promoName: name },
    });
  }
  findOne(id: number) {
    return this.promotionRepository.findOneOrFail({
      where: { id },
      relations: { promotionDetails: true },
    });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    const promotion = await this.promotionRepository.findOneOrFail({
      where: { id },
    });
    promotion.condition = updatePromotionDto.condition;
    promotion.start = new Date(updatePromotionDto.start);
    promotion.end = new Date(updatePromotionDto.end);
    const date = new Date();
    if (promotion.start < date && promotion.end > date) {
      promotion.status = true;
    } else {
      promotion.status = false;
    }
    promotion.promotionDetails = [];
    promotion.name = updatePromotionDto.name;
    promotion.discount = updatePromotionDto.discount;
    const me = JSON.parse(updatePromotionDto.promotionDetail.toString());
    for (const io of me) {
      const promoDetail = await this.promotionDetailRepository.findOneBy({
        id: io.id,
      });
      promoDetail.product = await this.productsRepository.findOneBy({
        id: io.productId,
      });
      promoDetail.productName = io.productName;
      promoDetail.promoName = updatePromotionDto.name;
      await this.promotionDetailRepository.save(promoDetail);
      promotion.promotionDetails.push(promoDetail);
    }
    if (updatePromotionDto.image && updatePromotionDto.image !== '') {
      promotion.image = updatePromotionDto.image;
    }
    await this.promotionRepository.save(promotion);
    const result = await this.promotionRepository.findOne({ where: { id } });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.promotionRepository.findOneOrFail({
      where: { id },
    });
    await this.promotionRepository.remove(deleteProduct);

    return deleteProduct;
  }
}
