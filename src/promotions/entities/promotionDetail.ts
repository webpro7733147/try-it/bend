import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Promotion } from './promotion.entity';
import { Product } from 'src/products/entities/product.entity';
@Entity()
export class PromotionDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @Column()
  promoName: string;

  @Column()
  productName: string;
  @ManyToOne(() => Promotion, (promotion) => promotion.promotionDetails, {
    onDelete: 'CASCADE',
  })
  promotion: Promotion;

  @ManyToOne(() => Product, (product) => product.promotionDetails)
  product: Product;
}
