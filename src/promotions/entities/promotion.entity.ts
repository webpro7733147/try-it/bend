import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PromotionDetail } from './promotionDetail';
@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  condition: string;

  @Column({ type: 'float' })
  discount: number;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column()
  start: Date;

  @Column()
  end: Date;

  @Column()
  status: boolean;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => PromotionDetail,
    (promotionDetail) => promotionDetail.promotion,
  )
  promotionDetails: PromotionDetail[];
  static start: Date;
}
