export class CreatePromotionDto {
  promotionDetail: {
    id: number;
    productId: number;
    promoName: string;
    productName: string;
  }[];
  condition: string;
  start: Date;
  end: Date;
  name: string;
  discount: number;
  image: string;
}
