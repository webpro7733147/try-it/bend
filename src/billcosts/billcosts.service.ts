import { Injectable } from '@nestjs/common';
import { CreateBillcostDto } from './dto/create-billcost.dto';
import { UpdateBillcostDto } from './dto/update-billcost.dto';
import { Billcost } from './entities/billcost.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BillcostsService {
  constructor(
    @InjectRepository(Billcost)
    private billcostsRepository: Repository<Billcost>,
  ) {}
  create(createBillcostDto: CreateBillcostDto) {
    return 'This action adds a new billcost';
  }

  findAll() {
    return this.billcostsRepository.find({
      relations: ['User', 'branch'],
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} billcost`;
  }

  update(id: number, updateBillcostDto: UpdateBillcostDto) {
    return `This action updates a #${id} billcost`;
  }

  remove(id: number) {
    return `This action removes a #${id} billcost`;
  }
}
