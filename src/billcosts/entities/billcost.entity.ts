import { Branch } from 'src/branchs/entities/branch.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Billcost {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  Typebill: string;

  @Column()
  Billtotal: number;

  @ManyToOne(() => User, (user) => user.billcosts)
  User: User;

  @Column()
  Date: Date;

  @Column({ type: 'time' })
  Time: Date;

  @ManyToOne(() => Branch, (branch) => branch.billcosts)
  @JoinColumn({ name: 'branchId' })
  branch: Branch;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;
}
