import { Module } from '@nestjs/common';
import { BillcostsService } from './billcosts.service';
import { BillcostsController } from './billcosts.controller';
import { Billcost } from './entities/billcost.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Billcost])],
  controllers: [BillcostsController],
  providers: [BillcostsService],
})
export class BillcostsModule {}
