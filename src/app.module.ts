import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { ProductsModule } from './products/products.module';
import { PromotionsModule } from './promotions/promotions.module';
import { MembersModule } from './members/members.module';
import { BranchsModule } from './branchs/branchs.module';
import { ReceiptsModule } from './receipts/receipts.module';
import { StocksModule } from './stocks/stocks.module';
import { BillcostsModule } from './billcosts/billcosts.module';
import { SalarysModule } from './salarys/salarys.module';
import { CheckinoutsModule } from './checkinouts/checkinouts.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Billcost } from './billcosts/entities/billcost.entity';
import { Checkinout } from './checkinouts/entities/checkinout.entity';
import { User } from './users/entities/user.entity';
import { Branch } from './branchs/entities/branch.entity';
import { Promotion } from './promotions/entities/promotion.entity';
import { Product } from './products/entities/product.entity';
import { PromotionDetail } from './promotions/entities/promotionDetail';
import { join } from 'path';
import { ServeStaticModule } from '@nestjs/serve-static';
@Module({
  imports: [
    // TypeOrmModule.forRoot({
    //   type: 'mysql',
    //   host: 'localhost',
    //   port: 3306,
    //   username: 'webpro',
    //   password: 'Pass@1234',
    //   database: 'webpro',
    //   entities: [Billcost, Checkinout, User, Branch], //อย่าลืมใส่ entities นะครับทุกคน
    //   synchronize: true,
    // }),
    //--------------sqlite-------------
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.sqlite',
      synchronize: true,
      logging: false,
      entities: [
        Billcost,
        Checkinout,
        User,
        Branch,
        Promotion,
        PromotionDetail,
        Product,
      ],
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    // ---------------------------------
    UsersModule,
    ProductsModule,
    PromotionsModule,
    MembersModule,
    BranchsModule,
    ReceiptsModule,
    StocksModule,
    BillcostsModule,
    SalarysModule,
    CheckinoutsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
