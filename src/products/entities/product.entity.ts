import { PromotionDetail } from 'src/promotions/entities/promotionDetail';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column()
  price: number;

  @Column()
  category: string;

  @Column({ default: '-' })
  type: string;

  @Column({ default: '-' })
  gsize: string;

  @Column({ default: '-' })
  sweet_level: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => PromotionDetail,
    (promotionDetail) => promotionDetail.promotion,
  )
  promotionDetails: PromotionDetail[];
}
