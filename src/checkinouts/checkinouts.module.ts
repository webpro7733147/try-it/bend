import { Module } from '@nestjs/common';
import { CheckinoutsService } from './checkinouts.service';
import { CheckinoutsController } from './checkinouts.controller';

@Module({
  controllers: [CheckinoutsController],
  providers: [CheckinoutsService],
})
export class CheckinoutsModule {}
