import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Checkinout {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
