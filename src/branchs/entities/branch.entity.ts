import { Billcost } from 'src/billcosts/entities/billcost.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  Address: string;

  @Column()
  tel: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => User, (user) => user.branch)
  manager: User[];

  @OneToMany(() => Billcost, (billcost) => billcost.branch)
  billcosts: Billcost[];
}
