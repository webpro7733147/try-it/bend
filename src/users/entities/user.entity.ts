import { Billcost } from 'src/billcosts/entities/billcost.entity';
import { Branch } from 'src/branchs/entities/branch.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  JoinColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 'noimage.png' })
  image: string;

  @Column()
  email: string;

  @Column({
    name: 'full-name',
    default: '',
  })
  fullName: string;

  @Column()
  password: string;

  @Column()
  gender: string;

  @Column()
  role: string;

  @ManyToOne(() => Branch, (branch) => branch.manager)
  @JoinColumn({ name: 'branch_id' })
  branch: Branch;

  @OneToMany(() => Billcost, (billcost) => billcost.User)
  billcosts: Billcost[];
}
